
/*Variables globales*/

let header$$ = document.createElement('header');
let nav$$ = document.createElement('nav');
let main$$ = document.createElement('main');
let footer$$ = document.querySelector('footer');
let arrayImg = [];

/** Array de categorias y url de la API**/
const categorias = 
[
    {
        text: "Personajes",
        url: 'https://rickandmortyapi.com/api/character',
      },
    {
        text: "Localizaciones",
        url: "https://rickandmortyapi.com/api/location",
    },
      {
        text: "Episodios",
        url: "https://rickandmortyapi.com/api/episode",
  
      }
]

/************************************** CABECERA ***************************************************************************/

function cabecera(){

  //Creación de nodos
  const tituloimg$$ = document.createElement('img');
  tituloimg$$.classList.add('titulo-img');
  const homeicono$$ = document.createElement('img');
  homeicono$$.classList.add('home');
  
  const cabecera$$ = document.createElement('div');
  cabecera$$.classList.add('main-header');

  //Creación de links del subtitulo
  for (const categoria of categorias) {
    let a$$ = document.createElement('a');
    a$$.setAttribute('href', '#');
    a$$.classList.add('langar');
    const link$$ = document.createTextNode(categoria.text);
    
    /**Escuchador**/
    a$$.addEventListener('click', function(e) {
      e.preventDefault();
      dibujaMain(a$$);
    });

    /**Escuchador**/
    homeicono$$.addEventListener('click', function(e) {
    reset();
    });

    a$$.appendChild(link$$);   
    nav$$.appendChild(a$$);
  }

  //Añado imagen de cabecera
  homeicono$$.src = './img/home-regular-60.png';
  tituloimg$$.src = './img/titulo.png';
  

  //Enlazando los nodos
  cabecera$$.appendChild(homeicono$$);
  cabecera$$.appendChild(tituloimg$$);
  header$$.appendChild(cabecera$$);
  header$$.appendChild(nav$$);

  document.body.insertBefore(header$$,footer$$);
}

/***************************************** MAIN ****************************************************************************/
/************************************************************************************************************************** */
/*Función del nav para llamar a las distintas categorias del main*/

function dibujaMain(a$$){
  nav$$.remove(); //Me quita el nav para que sea como si pasar a una pag nueva
  footer$$.remove();
  switch (a$$.text){
    case 'Personajes':
      dibujaPersonajes();
      break;
    case 'Localizaciones':
      tablaLocEpis();  
      dibujaLocalizaciones();
      break;
    case 'Episodios':
      eligeEpisodio();
      break;
  }
}

/****************************************** PERSONAJES ***************************************************************************** */
/*Función para dibujar lista de personajes (img y nombre) de cada uno */
function dibujaPersonajes(){

  fetch(categorias[0].url).then(res => res.json()).then(function(res){
  
  const div1$$ = document.createElement('div');
  const div2$$ = document.createElement('div');
  div1$$.classList.add('char-div');
  div2$$.classList.add('char-div');

  for (let i=0; i<8; i++){
    const div$$ = document.createElement('div');
    div$$.classList.add('char-div-div');
    const p$$ = document.createElement('p');
    p$$.classList.add('char-name');
    p$$.classList.add('langar');
    const img$$ = document.createElement('img');
    img$$.classList.add('char-img');

    p$$.textContent = res.results[i].name;
    img$$.setAttribute('src', res.results[i].image);
    div$$.appendChild(img$$);
    div$$.appendChild(p$$);

    /*Escuchador sobre el div de cada personaje para dibujar info-detalle de ese personaje*/
    div$$.addEventListener('click', function(e) {
      char_detail(res.results[i]);
    });

    if (i<4){
     div1$$.appendChild(div$$);
      
    }else{
      div2$$.appendChild(div$$);
    }
      
    main$$.appendChild(div1$$);
    main$$.appendChild(div2$$);
    document.body.appendChild(main$$);
  }
})
}
/* Función para dibujar detalle de cada personaje*/
function char_detail(myCharacter){
  const removeMeAll$$ = document.querySelectorAll('.char-div');
  for (const removeMe$$ of removeMeAll$$) {
    removeMe$$.remove(); 
  }

  const div1$$ = document.createElement('div');
  const div2$$ = document.createElement('div');
  div1$$.classList.add('char-div-info');
  div2$$.classList.add('char-div-txt');
  let cont = 1;

  for (const key in myCharacter){
      const p$$ = document.createElement('p');
      p$$.classList.add('char-info');
      p$$.classList.add('VT323');
      if(cont == 9){
        break;
      }else if( (cont === 1) ||  (cont === 5)){
        cont++;
      }else if((cont === 7) || (cont === 8)){
        p$$.textContent = (key.charAt(0).toUpperCase() + key.slice(1)) + ': ' + myCharacter[key].name;
        console.log( (key.charAt(0).toUpperCase() + key.slice(1)) + ': ' + myCharacter[key].name);
        cont++;
      }else{
        p$$.textContent = (key.charAt(0).toUpperCase() + key.slice(1)) + ': ' + myCharacter[key];
        console.log( (key.charAt(0).toUpperCase() + key.slice(1)) + ': ' + myCharacter[key]);
        cont++;
      }
      div2$$.appendChild(p$$);
  }
  const img$$ = document.createElement('img');
  img$$.classList.add('char-img-info');
    
  img$$.setAttribute('src', myCharacter.image);
        
  div1$$.appendChild(img$$);
  div1$$.appendChild(div2$$);

  main$$.appendChild(div1$$);
  document.body.appendChild(main$$);
}

/****************************************** LOCALIZACIONES ***************************************************************************** */

/*Función que me crea modelo tabla locs*/
function tablaLocEpis(){
  let index = 0;
  for(let i=0; i<3; i++){                 //Vamos a tener 3 filas
    const fila$$ = document.createElement('div');
    fila$$.classList.add('fila_main' + i);
    for(let j=0; j<4; j++){
      const elem$$ = document.createElement('div');
      elem$$.classList.add('elem-main-gen' + index);
      index++;
      fila$$.appendChild(elem$$);
    }
    main$$.appendChild(fila$$);
  }
  document.body.appendChild(main$$);
  
}

/*Función que dibuja localizaciones*/
function dibujaLocalizaciones(){
  fetch(categorias[1].url).then(res => res.json()).then(function(res){
    
    const fila1$$ = document.querySelector('.fila_main0');
    const fila3$$ = document.querySelector('.fila_main2');
    const fila2$$ = document.querySelector('.fila_main1');

    for (let i=0; i<12; i++){
      const p$$ = document.createElement('p');
      p$$.classList.add('langar');
      p$$.classList.add('loc-name');
      p$$.textContent = res.results[i].name;
      const elem$$ = document.querySelector('.elem-main-gen' + i);
      elem$$.appendChild(p$$);
      
      if(i < 4){
        fila1$$.appendChild(elem$$);
      }else if(i>7){
        fila3$$.appendChild(elem$$); 
      }else{
          fila2$$.appendChild(elem$$);
        }
      }
  })
}

/****************************************** EPISODIOS ***************************************************************************** */
/*Dibuja el input y el boton para que el usuario busque el episodio que quiera*/
function eligeEpisodio(){
  
  const divFormulario$$ = document.createElement('div');
  divFormulario$$.classList.add('.div_formulario');
  const div1$$ =  document.createElement('div');
  div1$$.classList.add('.div_h1_epis');
  const h1$$ = document.createElement('h1');
  h1$$.classList.add('h1_epis');
  h1$$.classList.add('langar');
  h1$$.textContent = ('¡Encuentra tu episodio!');
  div1$$.appendChild(h1$$);
  const divImg1$$ = document.createElement('div');
  divImg1$$.classList.add('div-img');
  const img1$$ = document.createElement('img');
  const img2$$ = document.createElement('img');
  const img3$$ = document.createElement('img');
  const img4$$ = document.createElement('img');
  img1$$.classList.add('img-roll');
  img2$$.classList.add('img-roll');
  img3$$.classList.add('img-roll');
  img4$$.classList.add('img-roll');
  
  
  for(let i=0; i<10; i++){
    arrayImg.push('./img/img' + i + '.jpg');
  }

  img1$$.src = arrayImg[0];
  img2$$.src = arrayImg[1];
  img3$$.src = arrayImg[2];
  img4$$.src = arrayImg[3];
  divImg1$$.appendChild(img1$$);
  divImg1$$.appendChild(img2$$);
  divImg1$$.appendChild(img3$$);
  divImg1$$.appendChild(img4$$);

  setInterval(function(){
    img1$$.remove();
    img2$$.remove();
    img3$$.remove();
    img4$$.remove();
    createImgEpi(img1$$, img2$$, img3$$, img4$$);
    divImg1$$.appendChild(img1$$);
    divImg1$$.appendChild(img2$$);
    divImg1$$.appendChild(img3$$);
    divImg1$$.appendChild(img4$$);
  }, 3000);
  
  const div2$$ =  document.createElement('div');
  div2$$.classList.add('div_formulario_epis');
  const h3$$ =  document.createElement('h3');
  h3$$.classList.add('h3_formulario_epis');
  h3$$.textContent = ('Escribe el nombre o el código del episodio del que quieres info');
  const input$$ =  document.createElement('input');
  input$$.classList.add('input_formulario_epis');
  const button$$ =  document.createElement('button');
  button$$.classList.add('button_formulario_epis');
  button$$.textContent = ('¡Búscalo!');

  /**Escuchador**/
  button$$.addEventListener('click', function(e) {
    if(!input$$.value){
      alert("Debes introducir un nombre o un código del episodio.");
    }else{
      epi_detail(input$$.value);
    }
  });
  
  div2$$.appendChild(h3$$);
  div2$$.appendChild(input$$);
  div2$$.appendChild(button$$);
 
  divFormulario$$.appendChild(div1$$);
  divFormulario$$.appendChild(div2$$);
  main$$.appendChild(divFormulario$$);
  main$$.appendChild(divImg1$$);

  document.body.appendChild(main$$);
}

/*Función que crea imagenes procedentes del array de imgenes*/
function createImgEpi(img1$$, img2$$, img3$$, img4$$){
  
  let rand = Math.round(Math.random()*9);  
  img1$$.src = arrayImg[rand];
  rand = Math.round(Math.random()*9);  
  img2$$.src = arrayImg[rand];
  rand = Math.round(Math.random()*9);  
  img3$$.src = arrayImg[rand];
  rand = Math.round(Math.random()*9);  
  img4$$.src = arrayImg[rand];
}

/*Busca el episodio con ese nombre o código y lo dibuja*/
function epi_detail(input$$){
  const nodos$$ = main$$.childNodes;
  const len = nodos$$.length;
  let noEncontrado = 1;
 
  for(let i=0; i<len; i++){
    if(nodos$$[0].childNodes !== undefined){
      const len2 = nodos$$[0].childNodes.length;
      for(let j=0; j<len2; j++){
        nodos$$[0].removeChild(nodos$$[0].childNodes[0]);
      }
    }
    nodos$$[0].remove();
  }

  const div$$ = document.createElement('div');
  fetch(categorias[2].url).then(res => res.json()).then(function(res){
  
  for (const episodio of res.results) {
    
    if(input$$.toLowerCase() == episodio.name.toLowerCase() || input$$.toLowerCase()==episodio.episode.toLowerCase()){

      //console.log(input$$.toLowerCase());
      //console.log(episodio.name.toLowerCase());
      //console.log(episodio.episode.toLowerCase());
      noEncontrado = 0;

      const p_name$$ = document.createElement('p');
      p_name$$.classList.add('p_name_epis');
      p_name$$.classList.add('langar');
      p_name$$.textContent = episodio.name;
      const p_date$$ = document.createElement('p');
      p_date$$.classList.add('p_date_epis');
      p_date$$.classList.add('langar');
      p_date$$.textContent = episodio.air_date;
      const p_code$$ = document.createElement('p');
      p_code$$.classList.add('p_code_epis');
      p_code$$.classList.add('langar');
      p_code$$.textContent = episodio.episode;
      
      div$$.appendChild(p_name$$);
      div$$.appendChild(p_date$$);
      div$$.appendChild(p_code$$);
      console.log('Dentro del fech y encontrado ' + noEncontrado);
    }else{
      noEncontrado = noEncontrado * 1;
      console.log('Dentro del fech y no encontrado ' + noEncontrado);
    }
  }
  if(noEncontrado === 1){
   
    eligeEpisodio();
    alert("El nombre o código del episodio no son correctos.");
  
  }
  main$$.appendChild(div$$);
  document.body.appendChild(main$$);
})
}

/******************************************************************************************************************** */

function reset(){
  location.reload();
  cabecera();
}

window.onload = function () { //Una vez que la página está cargada, llama a las funciones
    cabecera();
}